import Student from "./Student";
import Professor from "./Professor";


type User = Student | Professor
export default User