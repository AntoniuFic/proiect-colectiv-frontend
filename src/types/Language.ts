enum Language {
    RO = "RO",
    EN = "EN",
    GER = "GER",
    HUN = "HUN"
}

export default Language