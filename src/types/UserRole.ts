enum UserRole {
    STUDENT = "STUDENT",
    PROFESSOR = "PROFESSOR"
}

export default UserRole;
