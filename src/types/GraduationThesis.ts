type GraduationThesis = {
    id: number,
    professorCoordinatorId: number,
    thesisTitle?: string,
}

export default GraduationThesis