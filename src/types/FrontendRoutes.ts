enum FrontendRoutes {
    HOME = '/',
    LOGIN = '/login',
    STUDENT_PAGE = '/student',
    PROFESSOR_PAGE = '/professor',
    PROFESSOR_REQUESTS_PAGE = '/professor/requests',
    THESIS_PAGE = '/thesis-page',
}

export default FrontendRoutes;